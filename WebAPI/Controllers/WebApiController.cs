﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class WebApiController : ApiController
    {
        ZooDbEntities db = new ZooDbEntities();
        [System.Web.Http.HttpGet]
        public  IHttpActionResult GetData()
        {
            List<Animal> list = db.Animals.ToList();
            return Ok(list);
        }
        [System.Web.Http.HttpPost]
        public IHttpActionResult Insert(Animal ani)
        {
            db.Animals.Add(ani);
            db.SaveChanges();
            return Ok();
        }
        [System.Web.Http.HttpGet]
        public IHttpActionResult Details(int id)
        {
            Animal data = db.Animals.Where(x => x.Id == id).SingleOrDefault();
            return Ok(data);
        }
        [System.Web.Http.HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            Animal data = db.Animals.Where(x => x.Id == id).SingleOrDefault();
            db.Animals.Remove(data);
            db.SaveChanges();
            return Ok();

        }
    }
}
