﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class ConsumeController : Controller
    {
        HttpClient hc = new HttpClient();
        // GET: Consume
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Display()
        {
            List<Animal> list = new List<Animal>();
            hc.BaseAddress = new Uri("https://localhost:44389/Api/WebApi/GetData");
            var consume = hc.GetAsync("GetData");
            consume.Wait();
            var test = consume.Result;
            if (test.IsSuccessStatusCode)
            {
                var display = test.Content.ReadAsAsync<List<Animal>>();
                list = display.Result;
            }
            return View(list);
        }
        //polymorphism Concept
        [HttpGet]
        public ActionResult SendData()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SendData(Animal data)
        {
            hc.BaseAddress = new Uri("https://localhost:44389/Api/WebApi/Insert");
            var consume = hc.PostAsJsonAsync("Insert",data);
            consume.Wait();
            var test = consume.Result;
            if (test.IsSuccessStatusCode)
            {
                return RedirectToAction("Display");
            }
            else
            {
                return HttpNotFound();
            }

        }
        //delete data
        public ActionResult Animaldetails(int id)
        {
            Animal data = new Animal();
            hc.BaseAddress = new Uri("https://localhost:44389/Api/WebApi/Details");
            var consume = hc.GetAsync("Details?id=" + id.ToString());
            consume.Wait();
            var test = consume.Result;
            if (test.IsSuccessStatusCode)
            {
                var display = test.Content.ReadAsAsync<Animal>();
                display.Wait();
                data = display.Result;
            }
            return View(data);    
        }   
        public ActionResult Deletedata(int id)
        {
            Animal data = new Animal();
            hc.BaseAddress = new Uri("https://localhost:44389/Api/WebApi/Delete");
            var consume = hc.DeleteAsync("Delete?id=" + id.ToString());
            consume.Wait();
            var test = consume.Result;
            if (test.IsSuccessStatusCode)
            {
                return RedirectToAction("Display");

            }
            return View();
        }
    }
}